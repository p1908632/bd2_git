package polytech.bd2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import polytech.bd2.Salle;
/**
 *
 * @author elghazel
 */
@Entity	
@Table(name = "membre")
public class Membre {
    //Id du membre
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEMBRE_GEN")
    @SequenceGenerator(name = "MEMBRE_GEN", sequenceName = "membre_seq", allocationSize=1) 
    private int id;
    
    //Nom
    @Column(name="nom")
    private String nom;
    
    //Email
    @Column(name="email")
    private String email;
    
    //Liste des salles pour un membre
    @OneToMany(mappedBy="moderateur")
    private Collection<Salle> sallesdumoderateur = new ArrayList<Salle>();
    
    //Liste des messages pour un membre
    @OneToMany(mappedBy="auteur")
    private Collection<Message> messages = new ArrayList<Message>();
    
    @ManyToMany
    /*Jointure pour récupérer les salles où le membre a envoyé des messages*/
    @JoinTable(name = "message",
            joinColumns = {
                    @JoinColumn(name = "auteur")},
            inverseJoinColumns = {
                    @JoinColumn(name = "salle")})
    private Set<Salle> sallesDuMembre =  new HashSet<Salle>(); 
    
    /*Getter et setter*/
    
    

    public int getId() {
        return id;
    }
    
    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    //Récupérer les salles où le membre a envoyé un message
    public Set<Salle> getSallesDuMembre()
    {
        return sallesDuMembre;
    }
  
    //Récupérer les salles d'un modérateur
    public Collection<Salle> getSallesduModerateur() {
        return sallesdumoderateur;
    }
    
    public Collection<Message> getMessages()
    {
        return messages;
    }
    /*---------------------------------*/
    
    /*Fonctions pour assurer la cohérence des données*/
    public void addSalle(Salle s)
    {
        sallesdumoderateur.add(s);
    }
    
    public void removeSalle(Salle s)
    {
        sallesdumoderateur.remove(s);
    }
    
    public void addMessage(Message m)
    {
        messages.add(m);
    }
    
    public void removeMessage(Message m)
    {
        messages.remove(m);
    }
    /*------------------------------------------------*/
    
    

    
    
    
    
    
 	
}
