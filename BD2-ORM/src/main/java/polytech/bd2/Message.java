package polytech.bd2;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Message {

    @Id //Id du message
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGE_GEN")
    @SequenceGenerator(name = "MESSAGE_GEN", sequenceName = "message_seq", allocationSize=1)
    private int id;

    @ManyToOne //Liste des auteur
    @JoinColumn(name="auteur")
    private Membre auteur;

    @ManyToOne //Liste des salles
    @JoinColumn(name="salle")
    private Salle salle;

    @Column(name="date_envoi") //Date envoi
    private Timestamp date_envoi;


    @Column(name="titre")//Titre du message
    private String titre;

    @Column(name="corps") //Corps
    private String corps;


    @ManyToOne //Récupérer la liste des réponses des messages
    @JoinColumn(name="parent")
    private Message parent;
    @OneToMany(mappedBy="parent")
    private Collection<Message> reponses = new ArrayList<Message>();

}
