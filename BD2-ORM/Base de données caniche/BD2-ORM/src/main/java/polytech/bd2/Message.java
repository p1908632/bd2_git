/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package polytech.bd2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author camel
 */
@Entity

public class Message {
    //Id
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGE_SEQ")
    @SequenceGenerator(name = "MESSAGE_SEQ", sequenceName = "message_seq", allocationSize=1)
    private int id;
    
    //Liste des auteurs
    @ManyToOne
    @JoinColumn(name="auteur")
    private Membre auteur;
    
    //Liste des salles
    @ManyToOne
    @JoinColumn(name="salle")
    private Salle salle;

    //Date envoi
    @Column(name="date_envoi")
    private Timestamp date_envoi;
    
    //Titre du message
    @Column(name="titre")
    private String titre;
    
    //Corps 
    @Column(name="corps")
    private String corps;   
    
    //Récupérer la liste des réponses des messages
    @ManyToOne
    @JoinColumn(name="parent")
    private Message parent;
    @OneToMany(mappedBy="parent")
    private Collection<Message> reponses = new ArrayList<Message>();
    
    
    //Getter
    public Collection<Message> getReponses()
    {
        return reponses;
    }

    public Membre getAuteur() {
        return auteur;
    }

    public Salle getSalle() {
        return salle;
    }

    public String getTitre() {
        return titre;
    }

    public String getCorps() {
        return corps;
    }

    public Message getParent() {
        return parent;
    }
    /*------*/
    
    //Assurer la cohérence des données
    public void setAuteur(Membre auteur) {
        if(this.auteur!=null)
            this.auteur.removeMessage(this);
        
        this.auteur = auteur;
        this.auteur.addMessage(this);
    
    }

    public void setSalle(Salle salle) {
        if(this.salle!=null)
            this.salle.removeMessage(this);
        
        this.salle = salle;
        this.salle.addMessage(this);
    }
    
    
    
}
