package polytech.bd2;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author elghazel
 */
@Entity
public class Membre {

    @Id //Id du membre
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEMBRE_GEN")
    @SequenceGenerator(name = "MEMBRE_GEN", sequenceName = "membre_seq", allocationSize=1)
    private int id;


    @Column(name="nom") //Nom
    private String nom;


    @Column(name="email") //Email
    private String email;

    @OneToMany(mappedBy="moderateur") ////Liste des salles pour un membre
    private Collection<Salle> sallesdumoderateur = new ArrayList<Salle>();

    public Membre(String nom, String email)
    {
        this.nom = nom;
        this.email = email;
    }

    public int getId()
    {
        return id;
    }

    public Collection<Salle> getSallesduModerateur()
    {
        return sallesdumoderateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }



    public void setEmail(String email) {
        this.email = email;
    }

    public void addNewSalle(Salle salle)
    {
        sallesdumoderateur.add(salle);

    }
    public void removeSalle(Salle salle)
    {

        sallesdumoderateur.remove(salle);
    }
    public void changeSalle(Salle salle1, Salle salle2)
    {
        removeSalle(salle1);
        addNewSalle(salle2);
    }
}
