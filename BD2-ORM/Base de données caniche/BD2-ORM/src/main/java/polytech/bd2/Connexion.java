/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package polytech.bd2;

import java.sql.Connection;
import java.sql.SQLException;
import oracle.jdbc.pool.OracleDataSource;

/**
 *
 * @author elghazel
 */
public class Connexion {
    
    public Connection seConnecterOracle() throws SQLException {
        OracleDataSource ods = new OracleDataSource();
        ods.setDriverType("thin");
        ods.setServerName("epulgold");
        ods.setPortNumber(1521);
        ods.setDatabaseName("oraepul1");
        return ods.getConnection("APP4AINF00", "APP4AINF00");
    }
    
}
