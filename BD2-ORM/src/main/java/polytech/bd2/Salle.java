package polytech.bd2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author elghazel
 */
@Entity
public class Salle {

    @Id //Id de la salle
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALLE_GEN")
    @SequenceGenerator(name = "SALLE_GEN", sequenceName = "salle_seq", allocationSize=1)
    private int id;
    @Column(name="titre") //titre
    private String titre;

    @ManyToOne //moderateur (Membre)
    @JoinColumn(name = "moderateur")
    private Membre moderateur;

    public Salle(String titre, Membre moderateur) {
        this.titre = titre;
        setModerateur(moderateur);
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


    public Membre getModerateur() {
        return moderateur;

    }

    public void setModerateur(Membre moderateur)
    {
        if(this.moderateur != null) //si cette (this) salle à un modérateur
            this.moderateur.removeSalle(this); //on supprime cette (this) salle de la liste de l'ancien moderateur

        this.moderateur = moderateur;       //on change le moderateur de cette salle
        this.moderateur.addNewSalle(this);  //on ajoute à la liste du nouveau moderateur cette salle
    }

}
