package polytech.bd2;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
/**
 *
 * @author elghazel
 */
@Entity
public class Salle {
    //Id de la salle
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALLE_GEN")
    @SequenceGenerator(name = "SALLE_GEN", sequenceName = "salle_seq", allocationSize=1) 
    private int id;
    
    //titre
    @Column(name="titre")
    private String titre;
    
    //moderateur (Membre)
    @ManyToOne
    @JoinColumn(name = "moderateur")
    private Membre moderateur;
    
    //Liste des messages envoyé depuis une salle
    @OneToMany(mappedBy="salle")
    private Collection<Message> messages = new ArrayList<Message>();
    
    //Jointure pour récupérer les auteurs qui ont envoyé des messages dans cette salle
    @ManyToMany
    @JoinTable(name = "message",
            joinColumns = {
                    @JoinColumn(name = "salle")},
            inverseJoinColumns = {
                    @JoinColumn(name = "auteur")})
    private Set<Membre> auteursDeSalle =  new HashSet<Membre>();
    
    /*Getter et setter*/
    public int getId() {
        return id;
    }
 
    public String getTitre() {
        return titre;
    }
 
    public void setTitre(String titre) {
        this.titre = titre;
    }
 
    public Membre getModerateur() {
        return moderateur;
    }
    
    public Collection<Message> getMessages()
    {
        return messages;
    }
    
    //Retourne la liste des membres qui ont envoyé un message dans cette salle
    public Set<Membre> getAuteursDeSalle() {
        return auteursDeSalle;
    }
    /*---------------------*/
    
    
    /*Assurer la cohérence des données*/
     public void setModerateur(Membre moderateur) {
        if(this.moderateur!=null)
            this.moderateur.removeSalle(this);
        
        this.moderateur = moderateur;
        this.moderateur.addSalle(this);
    }
     
     public void addMessage(Message m)
    {
        messages.add(m);
    }
    
    public void removeMessage(Message m)
    {
        messages.remove(m);
    }
    /*----------------------------------------*/
    
    
    
    
 
    	
    
    
    

 
   
}


