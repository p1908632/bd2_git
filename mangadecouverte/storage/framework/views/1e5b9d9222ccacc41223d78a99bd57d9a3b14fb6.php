<table class="table table-bordered table-striped">
    <thead>
        <th>Id</th>
        <th>Titre</th>
        <th>Prix</th>
        <th>Couverture</th>
    </thead>
    <?php $__currentLoopData = $mangas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manga): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td> <?php echo e($manga->id_manga); ?> </td>
        <td> <?php echo e($manga->titre); ?> </td>
        <td> <?php echo e($manga->prix); ?> </td>
        <td> <?php echo e($manga->couverture); ?> </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table><?php /**PATH /home/hassen/websites/isi2/bd2_git/mangadecouverte/resources/views/index.blade.php ENDPATH**/ ?>