package polytech.bd2;
import polytech.bd2.Salle;
import polytech.bd2.Membre;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import junit.framework.TestCase;
 
/**
 *
 * @author elghazel
 */
public class TestORM extends TestCase {

    private EntityManager em;
 
    protected EntityManager createEntityManager() {
        return Persistence.createEntityManagerFactory("bd2_polytech_oracle").createEntityManager();
    }	


    @Override
    protected void setUp() throws Exception {
        if (em == null) {	
                    em = createEntityManager();
                }       
                em.getTransaction().begin();
    }

    @Override
    protected void tearDown() throws Exception {
            em.getTransaction().rollback();
            em.close();
    }

    /*Fonctions pour les membres*/
    private Membre createMembre(String nom, String email)
    {
        Membre m= new Membre();
        m.setEmail(email);
        m.setNom(nom);
        em.persist(m);
        return(m);
        
    }
    
    private Membre getMembre(int id)
    {
        return em.find(Membre.class, id);
    }
    
    private void supprimeMembre(int id)
    {
        Membre m = em.find(Membre.class, id);
        em.remove(m);
    }
    
    private void supprimeMembre(Membre m)
    {
        em.remove(m);
    }
    
    /*----------------------*/
    
    /*Fonctions pour les salles*/
    private Salle createSalle(String titre, Membre moderateur)
    {
        Salle s= new Salle();
        s.setTitre(titre);
        s.setModerateur(moderateur);
        em.persist(s);
        return(s);    
    }
    
    private Salle getSalle(int id)
    {
        return em.find(Salle.class, id);
    }
    
    private Message getMessage(int id)
    {
        return em.find(Message.class, id);
    }
    
    private void supprimeSalle(int id)
    {
        Salle s = em.find(Salle.class, id);
        em.remove(s);
    }
    
    private void supprimeSalle(Salle s)
    {
        em.remove(s);
    }
    /*-------------------------------*/
    
    
    

    public void testOperationsVariees() {
        Membre m = createMembre("Sting", "sting@police.com");
        assertNotNull(m);
        Membre m2 = getMembre(m.getId());
        assertEquals(m,m2);
        assertTrue(m==m2);
        if(m.getId()==m2.getId()) {
            System.out.println("dffd");
        }      
        Salle s = createSalle("Synchronicity", m);
        assertNotNull(s);
        Salle s2 = getSalle(s.getId());
        assertEquals(s,s2);
        assertTrue(s==s2);
        assertEquals(1,m.getSallesduModerateur().size()); 
        int sId = s.getId();
        supprimeSalle(sId);
        assertNull(getSalle(sId));
        int mId = m.getId();
        supprimeMembre(mId);
        assertNull(getMembre(mId));
        m = createMembre("Peter Gabriel", "peter@secretworld.org");
        s = createSalle("Live", m);
        mId = m.getId();
        sId = s.getId();
        supprimeSalle(s);
        supprimeMembre(m);
        assertNull(getSalle(sId));
        assertNull(getMembre(mId));
        /*Tests pour les messages*/
        m = getMembre(2);
        System.out.println("Nombre de messages de l'auteur "+m.getMessages().size());
        s = getSalle(2);
        System.out.println("Nombre de messages de la salle "+s.getMessages().size());
        Message me = getMessage(2);
        System.out.println("Nombre de réponses à un message "+me.getReponses().size());
        System.out.println("Nombre de salles où le membre a posté des messages "+m.getSallesDuMembre().size());
        System.out.println("Nombre d'auteurs de la salle "+s.getAuteursDeSalle().size());
    }
   
}
