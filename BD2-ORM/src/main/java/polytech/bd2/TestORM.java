package polytech.bd2;
import polytech.bd2.Salle;
import polytech.bd2.Membre;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import junit.framework.TestCase;

/**
 *
 * @author elghazel
 */
public class TestORM extends TestCase {

    private EntityManager em;

    protected EntityManager createEntityManager() {
        return Persistence.createEntityManagerFactory("bd2_polytech_oracle").createEntityManager();
    }

    public Membre createMembre(String nom, String email)
    {
        Membre membre = new Membre(nom, email);
        em.persist(membre);
        return(membre);
    }
    public Salle createSalle(String titre, Membre moderateur)
    {
        Salle salle = new Salle(titre, moderateur);
        em.persist(salle);
        return salle;
    }
    public Membre getMembre(int id)
    {
        return em.find(Membre.class, id);
    }

    public Salle getSalle(int id)
    {
        return em.find(Salle.class, id);
    }
    public void supprimeSalle(Salle salle)
    {
        em.remove(salle);
    }
    public void supprimeSalle(int id)
    {
        em.remove(getSalle(id));
    }

    public void supprimeMembre(Membre membre)
    {
        em.remove(membre);
    }
    public void supprimeMembre(int id)
    {
        em.remove(getMembre(id));
    }

    @Override
    protected void setUp() throws Exception {
        if (em == null) {
            em = createEntityManager();
        }
        em.getTransaction().begin();
    }

    @Override
    protected void tearDown() throws Exception {
        em.getTransaction().rollback();
    }


    public void testOperationsVariees() {

        Membre m = createMembre("Sting", "sting@police.com");
        assertNotNull(m);
        Membre m2 = getMembre(m.getId());
        assertEquals(m,m2);
        assertTrue(m==m2);
        if(m.getId()==m2.getId()) {
            System.out.println("dffd");
        }
        Salle s = createSalle("Synchronicity", m);
        assertNotNull(s);
        Salle s2 = getSalle(s.getId());
        assertEquals(s,s2);
        assertTrue(s==s2);
        assertEquals(1,m.getSallesduModerateur().size());
        int sId = s.getId();
        supprimeSalle(sId);
        assertNull(getSalle(sId));
        int mId = m.getId();
        supprimeMembre(mId);
        assertNull(getMembre(mId));
        m = createMembre("Peter Gabriel", "peter@secretworld.org");
        s = createSalle("Live", m);
        mId = m.getId();
        sId = s.getId();
        supprimeSalle(s);
        supprimeMembre(m);
        assertNull(getSalle(sId));
        assertNull(getMembre(mId));
    }
}
