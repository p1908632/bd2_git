<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Manga extends Model
{
    use HasFactory;

    //get the list of manga stored in the manga table
    public function getAll()
    {
        # code...
        $mangas = DB::table('manga')->get();
        return $mangas;
    }
}